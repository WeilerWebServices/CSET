# while statement
print "'while' example:"
i = 0
while i<5:
    print i,
    i+=1

# while with "break"
print "\n\n'while' example using 'break':"
i = 0
while i<5:
    print i,
    if i==2:
       break
    i+=1

# while with "continue"
print "\n\n'while' example using 'continue':"
i = -1
while i<5:
    i+=1
    if i==2:
       continue
    print i,