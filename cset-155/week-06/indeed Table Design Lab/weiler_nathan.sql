-- indeed

-- CREATE TABLEs

-- Id: Email, first_ame, LastName,

CREATE TABLE "User" (
    "UserId" INT NOT NULL,
    "Email" VARCHAR(60) NOT NULL,
    "LastName"  VARCHAR(120) NOT NULL,
    "FirstName"   VARCHAR(120) NOT NULL,    
    "Phone" VARCHAR(11),
    CONSTRAINT "PK_User" PRIMARY KEY  ("UserId")
    CONSTRAINT "FK_LastName” FOREIGN KEY ("LastName")
    REFERENCES "User" ("UserId")
);

-- Job: Type, Location, Salary

CREATE TABLE "Job" (
    "JobType" TEXT NOT NULL,
    "JobLocation"  FLOAT NOT NULL,
    "Salary" MONEY,
    "Benefits" MONEY,
    CONSTRAINT "PK_Job" PRIMARY KEY ("JobType")
    REFERENCES "Job" ("JobType")
);

-- Company: CompanyLocation, Salary

CREATE TABLE "Company" (
    "CompanyName" BIGSERIAL NOT NULL,
    "CompanyType" insert into table (column)
    "CompanyLocation" FLOAT,
    CONSTRAINT "FK_Company" FOREIGN KEY ("CompanyName")
    REFERENCES "Company"("CompanyName")
);

-- Schedule: Hours, PaidVacation, Bonuses

CREATE TABLE "Requirement" (
    "RequirementType" INT NOT NULL,
    "Education"  BOOLEAN NOT NULL,
    "Experience" BOOLEAN,
    "Skill" BOOLEAN,
    FOREIGN KEY ("RequirementType", "Experience")
);


-- Schedule: Hours, PaidVacation, Bonuses

CREATE TABLE "Schedule" (
    "UserId" INT NOT NULL,
    "Location"  FLOAT NOT NULL,
    "PaidVacation" BOOLEAN,
    "Benefits" BOOLEAN,
    "Hours" NOT NULL,
    PRIMARY KEY ("UserId", "Location")
    REFERENCES "Schedule"("UserId")
);

-- INSERT Table Data

INSERT into "User" ("Job", "Company", "Requirement", "Schedule")
SELECT JobType, Salary FROM Job,
SELECT CompanyType, CompanyLocation, FROM Company,
SELECT RequirementType, Experience, FROM Requirement, 
SELECT Benefits, Hours FROM Schedule;

INSERT into "Job" ("UserId", "Company", "Requirement", "Schedule")
SELECT UserId FROM User,
SELECT CompanyType FROM Company, 
SELECT Experience FROM Requirement,
SELECT PaidVacation FROM Schedule;

INSERT into "Company" ("UserId", "Job", "Requirement", "Schedule”)
SELECT Salary FROM Job,
SELECT RequirementType FROM Requirement,
SELECT Hours FROM Schedule;

INSERT into "Requirement" ("UserId", "Job", "Schedule")
SELECT LastName FROM UserId, 
SELECT JobType FROM Job, 
SELECT PaidVacation FROM Schedule;

INSERT into "Schedule" ("Job", "Company", "Requirement")
SELECT JobLocation FROM Job,
SELECT CompanyLocation FROM Company,
SELECT RequirementType FROM Requirement;
